# Please OCI in Gitlab

This project serves as a demo. It uses [Please](https://please.build) as a build
tool, builds a Go binary, packages it in an OCI image, can build the image
rootless in Gitlab CI, and pushes the image to Gitlab's container registry.

[This repo accompanies a blog post.](https://blog.awyeah.dev/posts/please-oci/)
